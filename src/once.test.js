import {once} from './once' 
var sinon = require("sinon")

describe ("My firts test with fake", ()=>{
  it("calls the original function", () => {
    var callback = sinon.fake();
    var proxy = once(callback);

    proxy()

    expect(callback.called).toBeTruthy()
  })

  it("calls the original function only once",  () => {
    var callback = sinon.fake();
    var proxy = once(callback);

    proxy()
    proxy()

    expect(callback.calledOnce).toBeTruthy()
    expect(callback.callCount).toBe(1)
  })

  it("calls original function with right this and args",  () => {
    var callback = sinon.fake();
    var proxy = once(callback);
    var obj = {};

    proxy.call(obj, 1, 2, 3);

    expect(callback.calledOn(obj)).toBeTruthy()
    expect(callback.calledWith(1, 2, 3)).toBeTruthy()
  })
})
