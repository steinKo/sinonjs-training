import {greet} from './greet'
const sinon = require("sinon");
 
describe("testing the greeter", () => {
  it("checks the greet function", () => {
	var clock = sinon.useFakeTimers(new Date(2022, 0, 6));
    expect(greet('Alice')).toBe('Hello, Alice! Today is Thursday, January 6, 2022');
    clock.restore();
  });
});